import os
import sys
import time
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv

methods=['vq','dl']
accel=[10,20,30,40]


for method in methods:
    for a in accel:
        root="/home/stavros/Workspace/face-alignment-pytorch/checkpoint"

        my_data = np.genfromtxt(root+'_'+method+'_'+str(a)+'/log.txt', delimiter='	')
        # print(my_data)

        # with open(root+'/log.txt', newline='\n') as csvfile:
        #     filereader = csv.reader(csvfile, delimiter='	', quotechar='|')
            # for row in filereader:
            #     print(','.join(row))

        my_data=my_data[1:,:-1]

        values=my_data[-9:,-1]

        print(values)
